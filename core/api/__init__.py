from flask import Blueprint

v1 = Blueprint('api', __name__, url_prefix='/api/v1')

from .check_population import views
