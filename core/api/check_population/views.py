from flask import jsonify, request
from core.api import v1
from core.api.utils import get_all_points, prepare_value
from settings import DATASET


def get_population(lat, lon, radius):
    total_population = 0
    all_points = get_all_points(DATASET)
    # Store pow(radius, 2) in variable to avoid unneed calculation in each iteration
    # during for loop.
    radius_pow_2 = radius * radius
    for p in all_points:
        # Check if each point belongs to circle
        if (p[0]-lat)*(p[0]-lat)+(p[1]-lon)*(p[1]-lon) <= radius_pow_2:
            # Increase value of total population with value of point inside circle
            total_population += p[2]
    
    return total_population


@v1.route('/population', methods=['GET'])
def check_population():
    params = request.args
    # Check if all required parameters were provided.
    if not all([field in params for field in ('latitude', 'longtitude', 'radius')]):
        return jsonify({"error": 'Parameters latitude, longtitude and radius are required.'}), 400

    error_lat, latitude   = prepare_value(params['latitude'], 'latitude')
    error_lon, longtitude = prepare_value(params['longtitude'], 'longtitude')
    error_rad, radius     = prepare_value(params['radius'], 'radius')

    if any((error_lat, error_lon, error_rad)):
        return jsonify({"error":  error_rad or error_lon or error_lat}), 400
    else:
        return jsonify({"population": get_population(latitude, longtitude, radius)})
