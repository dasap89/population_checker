from settings import VALUES_VALIDATOR

def memodict(f):
    """ Memoization decorator for a function taking a single argument """
    class memodict(dict):
        def __missing__(self, key):
            ret = self[key] = f(key)
            return ret 
    return memodict().__getitem__


@memodict
def get_all_points(filename):
    all_points = set()
    with open(filename, 'r') as source:
        for line in source:
            fields = line.split(';')
            all_points.add(tuple([float(fields[1]), float(fields[2]), int(fields[3])]))
    return all_points


def mapper(value, unit):
    return {
        "latitude": lambda: float(value),
        "longtitude": lambda: float(value),
        "radius": lambda: float(value) / 111,
    }.get(unit, lambda: None)()


def is_valid(value, unit):
    if not value:
        return False

    answer = True
    conditions = VALUES_VALIDATOR[unit]
    for cond in conditions:
        if cond == 'min':
            answer &= value >= conditions[cond]
        if cond == 'max':
            answer &= value <= conditions[cond]
    return answer


def prepare_value(value, unit):
    if unit in VALUES_VALIDATOR:
        error, val = VALUES_VALIDATOR[unit]['error'], None
        try:
            val = mapper(value, unit)
            error = None if is_valid(val, unit) else error
        except (ValueError, TypeError):
            pass

        return error, val
    else:
        raise ValueError("Unknown units of measurement.")
