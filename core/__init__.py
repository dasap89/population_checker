import os

from flask import Flask
from flask_script import Manager

import settings


app = Flask(__name__)

envs = {
    'develop': settings.Develop
}
ENV = os.environ.get('ENV', 'develop')

if ENV in envs:
    app.config.from_object(envs[ENV])
else:
    raise EnvironmentError('No config specified')

manager = Manager(app)

from core import api
app.register_blueprint(api.v1)
