# -*- coding: utf-8 -*-
import os

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
DATASET = os.path.join(BASE_DIR, "data", "allCountries.csv")

class Develop(object):
    DEBUG = True
    SECRET_KEY = 'population_informer'


VALUES_VALIDATOR = {
    "latitude": {
        "min": -90,
        "max": 90,
        "error": "Parameter `latitude` is invalid.",
    },
    "longtitude": {
        "min": -180,
        "max": 180,
        "error": "Parameter `longtitude` is invalid.",
    },
    "radius": {
        "min": 0,
        "error": "Parameter `radius` is invalid.",
    }
}
