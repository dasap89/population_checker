# This is the main file with tests
import os
import unittest
import tempfile
from core import app

class PopulationTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.latitude = 49.5445
        self.longtitude = 29.1393
        self.radius = 155.4

    def test_population_get_method_with_valid_params(self):
        resp = self.app.get(
            '/api/v1/population?'
            f'latitude={self.latitude}&'
            f'longtitude={self.longtitude}&'
            f'radius={self.radius}'
        )
        self.assertEqual(resp.json['population'], 6888687)

    def test_population_post_method_return_data(self):
        resp = self.app.post(
            '/api/v1/population',
            data={
                "latitude": self.latitude,
                "longtitude": self.longtitude,
                "radius": self.radius
            }
        )
        # Should return method not allowed
        self.assertEqual(resp.status_code, 405)

    def test_population_without_required_params(self):
        resp = self.app.get(f'/api/v1/population')
        self.assertEqual(resp.status_code, 400)
        self.assertDictEqual(
            resp.json, 
            {"error": "Parameters latitude, longtitude and radius are required."}
        )

    def test_population_with_only_one_param(self):
        resp = self.app.get(f'/api/v1/population?latitude={self.latitude}')
        self.assertEqual(resp.status_code, 400)
        self.assertDictEqual(
            resp.json, 
            {"error": "Parameters latitude, longtitude and radius are required."}
        )

    def test_population_with_invalid_parameter(self):
        resp = self.app.get('/api/v1/population?invalid_param=invalid_value')
        self.assertEqual(resp.status_code, 400)
        self.assertDictEqual(
            resp.json, 
            {"error": "Parameters latitude, longtitude and radius are required."}
        )

    def test_population_with_invalid_latitude(self):
        resp = self.app.get(
            '/api/v1/population?'
            'latitude=999&'
            f'longtitude={self.longtitude}&'
            f'radius={self.radius}'
        )
        self.assertEqual(resp.status_code, 400)
        self.assertDictEqual(
            resp.json, 
            {"error": "Parameter `latitude` is invalid."}
        )

    def test_population_with_invalid_longtitude(self):
        resp = self.app.get(
            '/api/v1/population?'
            f'latitude={self.latitude}&'
            'longtitude=999&'
            f'radius={self.radius}'
        )
        self.assertEqual(resp.status_code, 400)
        self.assertDictEqual(
            resp.json, 
            {"error": "Parameter `longtitude` is invalid."}
        )

    def test_population_with_invalid_radius(self):
        resp = self.app.get(
            '/api/v1/population?'
            f'latitude={self.latitude}&'
            f'longtitude={self.longtitude}&'
            'radius=abc'
        )
        self.assertEqual(resp.status_code, 400)
        self.assertDictEqual(
            resp.json, 
            {"error": "Parameter `radius` is invalid."}
        )

    def test_population_with_empty_latitude(self):
        resp = self.app.get(
            '/api/v1/population?'
            'latitude=""&'
            f'longtitude={self.longtitude}&'
            f'radius={self.radius}'
        )
        self.assertEqual(resp.status_code, 400)
        self.assertDictEqual(
            resp.json, 
            {"error": "Parameter `latitude` is invalid."}
        )

    def test_population_with_coordinates_of_ocean(self):
        resp = self.app.get(
            '/api/v1/population?'
            'latitude=45.497832&'
            'longtitude=-31.149304&'
            f'radius={self.radius}'
        )
        self.assertEqual(resp.json['population'], 0)


if __name__ == '__main__':
    unittest.main()