# Population checker
This is a test task:

Please design and implement a web API in Python to tell the population living within an area. The API should take a latitude, longitude and radius

WARNING: The API should to work globally, but don't use a 3rd party library like uszipcode to do this, otherwise the task becomes too trivial.

## How to run the application?

To run application locally you should do next:

1. \[Needed only during first run\]: Check "How to run application first time?"
2. Go to folder with application.
3. Activate virtual environment.

    ```$ source env/bin/activate```

4. Run command to start the application:

    ```(env) $ python app.py```

Application will serve on port 5005.


## How to run application first time?

1. Clone repo from remote to this folder:

    ```$ git clone https://dasap89@bitbucket.org/dasap89/population_checker.git```

2. Create virtual environment with python3.6 inside and activate it:

    ```$ virtualenv env --python=python3.6```

    ```$ source env/bin/activate```

3. Install packages from file with requirements.

    ``` (env) $ pip install -r requirements.txt```

4. Now you can run application via command:
 
    ```(env) $ python app.py```

## How to test the application?

1. \[Optional\]: Activate virtual environment:

    ```$ source env/bin/activate```

2. Run command:

    ```(env) $ python tests.py```

## Where to see deployed application?

Currently the application is available by next url:

http://shelby89.pythonanywhere.com/

## Endpoints

There is next endpoint in web app:

[GET] api/v1/population
with required parameters:
- latitude   - in degrees
- longtitude - in degrees
- radius     - in kilometers

For example command (or just go by [link](http://shelby89.pythonanywhere.com/api/v1/population?latitude=49.5445&longtitude=29.1393&radius=155.4)):

```curl "http://shelby89.pythonanywhere.com/api/v1/population?latitude=49.5445&longtitude=29.1393&radius=155.4"```

will return next value:

```{"population": 6888687"}```